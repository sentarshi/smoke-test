package com.fishbowlapp.run;

import com.fishbowlapp.framework.MicroFramewok;
import com.fishbowlapp.smoketest.*;
import com.testinium.deviceinformation.exception.DeviceNotFoundException;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.qameta.allure.Description;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
@Test
public class SmokeTest {
    private String reportDirectory = "reports";
    private String reportFormat = "xml";
    private String testName = "RegistrationAndroid";
    protected AndroidDriver<AndroidElement> driver = null;

    @BeforeSuite
    public void setUp() throws InterruptedException, IOException, DeviceNotFoundException {
        DesiredCapabilities dc = new DesiredCapabilities();
       //dc.setCapability(MobileCapabilityType.APP,"C:\\Program Files (x86)\\Jenkins\\workspace\\Android Smoke Test\\APK\\Fishbowl.apk");
        dc.setCapability("reportDirectory", reportDirectory);
        dc.setCapability("reportFormat", reportFormat);
        dc.setCapability("testName", testName);
        dc.setCapability("deviceName",  MicroFramewok.deviceInfo().getName());
        dc.setCapability("automationName", "UiAutomator2");
        dc.setCapability("disableWindowAnimation","true");
        dc.setCapability("newCommandTimeout","150");
        //dc.setCapability("skipServerInstallation","true");
        //dc.setCapability("instrumentApp",true);
        dc.setCapability(MobileCapabilityType.UDID, MicroFramewok.deviceInfo().getId());
        dc.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.fishbowlmedia.fishbowl.dev");
        dc.setCapability("nativeInstrumentsLib",true);
        dc.setCapability("adbExecTimeout","50000");
       //dc.setCapability("noReset",true);
        //dc.setCapability("app","C:\\Users\\Vladimir\\Downloads\\Fishbowl-5.8.2(433)2-qa.apk");
        dc.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.fishbowlmedia.fishbowl.activities.SplashActivity");
        driver = new AndroidDriver<>(new URL("http://localhost:4723/wd/hub"), dc);
        driver.setLogLevel(Level.INFO);
        String con = driver.getContext();

    }
    @Test
    @Description ("Registration standard flow")
    public void Registration() throws InterruptedException, IOException {
    RegistrationAndroid registration = new RegistrationAndroid(driver);
    registration.TestAssemble();
}
    @Test
    @Description ("Creation of a new custom bowl")
    public void BowlCreate() throws InterruptedException {
        driver.closeApp();
        try {
            driver.startActivity(new Activity("com.fishbowlmedia.fishbowl.dev", "com.fishbowlmedia.fishbowl.activities.SplashActivity"));
        } catch (Exception e) {
        }
        CreateNewBowl newbowl = new CreateNewBowl(driver);
        newbowl.TestAssemble();
    }
    @Test
    @Description ("Creation of a new bowl post")
    public void BowlPostCreate() throws InterruptedException {
        ComposePost newpostbowl = new ComposePost(driver);
        newpostbowl.TestAssembly();
    }
    @Test
    @Description("Create comments and Likes")
    public void CommentsAndLikes() throws InterruptedException, IOException {
        Like commentslikes = new Like(driver);
        commentslikes.TestAssembly();
    }
    @Test
    @Description("Edit Profiles")
    public void EditProfile() throws InterruptedException, IOException {
        driver.closeApp();
        try {
            driver.startActivity(new Activity("com.fishbowlmedia.fishbowl.dev", "com.fishbowlmedia.fishbowl.activities.SplashActivity"));
        } catch (Exception e) {
        }
        Profile_Test editprofile = new Profile_Test(driver);
        editprofile.TestAssembly();
    }
    @Test
    @Description("Bookmarks")
    public void Bookmarks() throws InterruptedException, IOException {
        driver.closeApp();
        try {
            driver.startActivity(new Activity("com.fishbowlmedia.fishbowl.dev", "com.fishbowlmedia.fishbowl.activities.SplashActivity"));
        } catch (Exception e) {
        }
        Bookmarks bookmark = new Bookmarks(driver);
        bookmark.TestAssemble();
    }
    @Test
    @Description ("Dm Test")
    public void DMTest() throws InterruptedException, IOException {
        driver.closeApp();
        try {
            driver.startActivity(new Activity("com.fishbowlmedia.fishbowl.dev", "com.fishbowlmedia.fishbowl.activities.SplashActivity"));
        } catch (Exception e) {
        }
        DM_Test dmtest = new DM_Test(driver);
        dmtest.TestAssembly();
    }
    @Test
    @Description ("Registration Education")
    public void RegistrationEdu() throws InterruptedException, IOException {
        Registration_Edictaion registration = new Registration_Edictaion(driver);
        registration.TestAssemble();}

    @Test
    @Description ("Registration standard flow")
    public void RegistrationRestricted() throws InterruptedException, IOException {
        Registration_restricted registration = new Registration_restricted(driver);
        registration.TestAssemble();
    }

    @Test
    @Description ("Registration standard flow")
    public void LoginSuspended() throws InterruptedException, IOException {
        LogIn_Suspended registration = new LogIn_Suspended(driver);
        registration.TestAssemble();
    }

}