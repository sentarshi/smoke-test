package com.fishbowlapp.framework;

import com.ansgar.adbdeviceinfoparser.AdbDeviceInfo;
import com.ansgar.adbdeviceinfoparser.Device;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MicroFramewok {
    protected AndroidDriver<AndroidElement> driver = null;
    public MicroFramewok(AndroidDriver<AndroidElement> driverinput) {
        driver = driverinput;
    }

    public MicroFramewok() {

    }


    public static void WaitForElement(AndroidDriver<AndroidElement> driver, By selector) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(selector));
    }


    public void VerticalSwipe (AndroidDriver driver, int times) throws InterruptedException {
        Dimension size = driver.manage().window().getSize();
        int startx = (int) (size.height * 0.80);
        int endx = (int) (size.height * 0.30);
        int starty = size.width / 2;

        for (int i = 0; i < times; i++) {

            new TouchAction<>(driver).press(PointOption.point(starty, endx)).waitAction().moveTo(PointOption.point(starty, startx)).release().perform();

        }
    }

    public void VerticalSwipeDownUp (AndroidDriver driver, int times) throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        Dimension size = driver.manage().window().getSize();
        int startx = (int) (size.height * 0.30);
        int endx = (int) (size.height * 0.80);
        int starty = size.width / 2;

        for (int i = 0; i < times; i++) {

            new TouchAction<>(driver).press(PointOption.point(starty, endx)).waitAction().moveTo(PointOption.point(starty, startx)).release().perform();

        }
    }

    public static boolean TryCElementPresent (AndroidDriver<AndroidElement> driver, By selector) {
        try {
            new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(selector));
            return true;
        }
        catch (Exception e) {
            return false;
        }

    }

    public static Device deviceInfo() {
        AdbDeviceInfo deviceInfo = new AdbDeviceInfo();
        List<Device> devices = deviceInfo.getDevices();
        if (devices.size() > 0) {
            Device device = devices.get(0);
            System.out.println(device.toString());
            return device;
        }
        return new Device();
    }


        DateFormat df;
    String folder_name;
    public void captureScreenShots() throws IOException {
       folder_name = "ScreenShot";
        File f=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        //create dir with given folder name
        new File(folder_name).mkdir();
        //Setting file name
        String file_name = df.format(new Date()) + ".png";
        //coppy screenshot file into screenshot folder.
        FileUtils.copyFile(f,new File(folder_name + "/" + file_name));
    }

}
