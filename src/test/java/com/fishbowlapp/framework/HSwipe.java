package com.fishbowlapp.framework;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.Dimension;

public class HSwipe {
    protected AndroidDriver<AndroidElement> driver = null;
    private void HorizontalSwipe(int times) throws InterruptedException {
        Dimension size = driver.manage().window().getSize();
        int startx = (int) (size.width * 0.90);
        int endx = (int) (size.width * 0.10);
        int starty = size.height / 2;

        for (int i = 0; i < times; i++) {

            new TouchAction<>(driver).press(PointOption.point(startx, starty)).waitAction().moveTo(PointOption.point(endx, starty)).release().perform();

        }
    }

    public void HSwipe (int times) throws InterruptedException {
        this.HorizontalSwipe(times);
    }


}