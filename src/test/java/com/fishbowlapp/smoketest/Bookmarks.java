package com.fishbowlapp.smoketest;

import com.fishbowlapp.framework.MicroFramewok;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Bookmarks {
    protected AndroidDriver<AndroidElement> driver = null;
    public Bookmarks(AndroidDriver<AndroidElement> driverinput) {
        driver = driverinput;}
    @Attachment(value = "Page screenshot", type = "image/png")
    public byte[] saveScreenshotPNG(WebDriver driver){
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
    }

    String temp = null;

    @Step("Into Feed Post")
    private void IntoFeedPost() throws InterruptedException {
        MicroFramewok.WaitForElement(driver,By.xpath("//android.widget.RelativeLayout[contains(@resource-id,'root_container_rl')]"));
    driver.findElement(By.xpath("//android.widget.RelativeLayout[contains(@resource-id,'root_container_rl')]")).click();

    }

    @Step("Add to bookmarks")
    private void AddtoBockmarks() throws InterruptedException {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/menu_book_mark_off_iv"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/menu_book_mark_off_iv")).click();
        temp = driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/item_message_tv")).getAttribute("text");
        driver.findElement(MobileBy.AccessibilityId("Navigate up")).click();
    }

    @Step("Goto Profile Screen")
    private void GoToProfileScreen() throws InterruptedException {
        MicroFramewok.WaitForElement(driver,By.xpath("//*[@text='Profile']"));
        driver.findElement(By.xpath("//*[@text='Profile']")).click();
    }


    @Step("Goto Bookmarks Tab")
    private void GoToBookmarksTab() throws InterruptedException {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/bookmarks"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/bookmarks")).click();
        String result = driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/item_message_tv")).getAttribute("text");
        Assert.assertEquals(result, temp);
    }

    @Test
    @Step("Bookmarks test")
    public void TestAssemble() throws InterruptedException {
        this.IntoFeedPost();
        this.AddtoBockmarks();
        this.GoToProfileScreen();
        this.GoToBookmarksTab();
        this.saveScreenshotPNG(driver);
    }

}
