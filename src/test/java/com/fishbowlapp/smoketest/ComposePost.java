package com.fishbowlapp.smoketest;

import com.fishbowlapp.framework.MicroFramewok;
import com.fishbowlapp.framework.HSwipe;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.qameta.allure.Attachment;
import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class ComposePost {
    protected AndroidDriver<AndroidElement> driver = null;
    public ComposePost(AndroidDriver<AndroidElement> driverinput) {
        driver = driverinput;}
    String lorem = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.";
    @Attachment(value = "Page screenshot", type = "image/png")
    public byte[] saveScreenshotPNG(WebDriver driver){
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
    }

    @Step("GotoBowlsScreen")
    private void GoToBowlScreen() throws InterruptedException  {
        MicroFramewok.WaitForElement(driver,By.xpath("//*[@text='Bowls']"));
        driver.findElement(By.xpath("//*[@text='Bowls']")).click();
    }
    @Step("Navigate into Bowl")
    private void NavigateIntoBowl() throws InterruptedException {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/bowl_name_tv"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/bowl_name_tv")).click();}

    @Step("Select Compose")
    private void SelectComposeField() throws InterruptedException {
        MicroFramewok.WaitForElement(driver, MobileBy.AccessibilityId("Compose"));
        driver.findElement(MobileBy.AccessibilityId("Compose")).click();
    }

    @Step("Community Rules")
    private void CommunityRules() throws InterruptedException {
        driver.getPageSource();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        new HSwipe().HSwipe(3);
        new WebDriverWait(driver, 1).until(ExpectedConditions.presenceOfElementLocated(By.id("com.fishbowlmedia.fishbowl.dev:id/got_it_tv")));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/got_it_tv")).click();}

    @Step("Enter Text")
    private void EnterText() {
        driver.getPageSource();
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/message_et"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/message_et")).sendKeys(lorem);
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/publish_tv")).click();
    }
    @Step
    @Description("Verify that post is placed correctly")
    private void VerifyPostText() {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/item_message_tv"));
        String result = driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/item_message_tv")).getAttribute("text");
        Assert.assertEquals(result, lorem);
    }

    @Step("Edit Re Reply message")
    private void EditPost() {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/more_ic"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/more_ic")).click();
        driver.findElement(By.xpath("//*[@text='Edit Message']")).click();
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/message_et")).sendKeys("EDITED POST HERE");
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/publish_tv")).click();
    }

    @Step("Remove Re Reply message")
    private void RemoveRereply() {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/more_ic"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/more_ic")).click();
        driver.findElement(By.xpath("//*[@text='Remove Message']")).click();

    }


    @Step("Report Post")
    private void ReportPost() {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/more_ic"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/more_ic")).click();
        driver.findElement(By.xpath("//*[@text='Report']")).click();
        driver.findElement(MobileBy.AccessibilityId("spam")).click();
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/submit")).click();
    }


    @Test
        @Description("Write a post in Bowl")
        public void TestAssembly() throws InterruptedException
    {
        if (driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/welcomeToBowlRoot")).isDisplayed() )
        {
            for (int i = 0; i <3; i++) {
                this.SelectComposeField();
                try {
                    this.CommunityRules();
                } catch (Exception e) {
                }
                this.EnterText();
            }
            //this.VerifyPostLimit();
            this.VerifyPostText();
            this.EditPost();
            this.saveScreenshotPNG(driver);
            this.ReportPost();
        }

        else
        {
            this.GoToBowlScreen();
            this.NavigateIntoBowl();
            for (int x = 0; x <3; x++) {
                this.SelectComposeField();
                this.saveScreenshotPNG(driver);
                try {
                    this.CommunityRules();
                    this.saveScreenshotPNG(driver);
                } catch (Exception e) {
                }
                this.EnterText();
            }
            this.VerifyPostText();
            this.EditPost();
            this.saveScreenshotPNG(driver);
            this.RemoveRereply();
            this.ReportPost();
            this.saveScreenshotPNG(driver);
        }
    }

}
