package com.fishbowlapp.smoketest;

import com.fishbowlapp.framework.MicroFramewok;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Profile_Test {
    protected AndroidDriver<AndroidElement> driver = null;
    public Profile_Test(AndroidDriver<AndroidElement> driverinput) {
        driver = driverinput;}
    @Attachment(value = "Page screenshot", type = "image/png")
    public byte[] saveScreenshotPNG(WebDriver driver){
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
    }

    @Step("Goto Profile Screen")
    public void GoToProfileScreen() throws InterruptedException {
        MicroFramewok.WaitForElement(driver,By.xpath("//*[@text='Profile']"));
        driver.findElement(By.xpath("//*[@text='Profile']")).click();

    }

    @Step("Profile tutorial")
    public void ProfileTutorial() throws InterruptedException {
        MicroFramewok.WaitForElement(driver,By.className("android.widget.LinearLayout"));
        driver.findElement(By.className("android.widget.LinearLayout")).click();
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/sign_type_title_tv"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/sign_type_title_tv")).click();
        MicroFramewok.WaitForElement(driver,MobileBy.AccessibilityId("Love"));
        driver.findElement(MobileBy.AccessibilityId("Love")).click();
        driver.findElement(MobileBy.AccessibilityId("Navigate up")).click();

    }

    @Step("Go To edit Profile screen")
    public void EditProfile() throws InterruptedException {
        driver.pressKey(new KeyEvent(AndroidKey.ENTER));
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/edit_profile_tv"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/edit_profile_tv")).click();

    }

    @Step("Change Company name")
    public void EditCompanyName() throws InterruptedException {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/user_company_name_tv"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/user_company_name_tv")).click();
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/editText"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/editText")).click();
        String mail = "test" + Math.random() + "@yadntest.com";
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/editText")).setValue(mail);
        driver.pressKey(new KeyEvent(AndroidKey.ENTER));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/user_company_name_tv"));
        String result = driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/user_company_name_tv")).getAttribute("text");
        Assert.assertEquals(result,"Itech");
    }
    @Step("Change Crowd and Division name")
    public void EditCrowdDivision() throws InterruptedException {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/user_crowd_name_tv"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/user_crowd_name_tv")).click();
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/md_buttonDefaultPositive")).click();
        new WebDriverWait(driver, 3).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='Advisory']")));
        driver.findElement(By.xpath("//*[@text='Advisory']")).click();
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/user_division_name_tv"));
        String result = driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/user_division_name_tv")).getAttribute("text");
        Assert.assertEquals(result,"Advisory");
    }
    @Step("Change Location")
    public void EditLocation() throws InterruptedException {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/user_location_tv"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/user_location_tv")).click();
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/editText"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/editText")).click();
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/editText")).sendKeys("New York");
        MicroFramewok.WaitForElement(driver,By.xpath("//*[@text='New York, NY, United States']"));
        driver.findElement(By.xpath("//*[@text='New York, NY, United States']")).click();
        driver.findElement(By.xpath("//*[@text='SAVE']")).click();
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/user_location_tv"));
        String result = driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/user_location_tv")).getAttribute("text");
        Assert.assertEquals(result,"New York");

    }
    @Step("Change Website")
    public void EditWebsite() throws InterruptedException {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/user_link_tv"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/user_link_tv")).click();
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/user_link_tv")).sendKeys("www.testfish.com");
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/editProfileDoneItem")).click();
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/edit_profile_tv"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/edit_profile_tv")).click();
        String result = driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/user_link_tv")).getAttribute("text");
        Assert.assertEquals(result, "www.testfish.com");
    }
    @Step("Change About")
    public void EditAbout() throws InterruptedException {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/user_about_me_et"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/user_about_me_et")).click();
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/user_about_me_et")).sendKeys("Some personal info about user is placed here");
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/editProfileDoneItem")).click();
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/edit_profile_tv"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/edit_profile_tv")).click();
        String result = driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/user_about_me_et")).getAttribute("text");
        Assert.assertEquals(result, "Some personal info about user is placed here");
    }



    @Test
    @Step("User Profile Edit")
    public void TestAssembly() throws InterruptedException {
        this.GoToProfileScreen();
        this.ProfileTutorial();
        this.EditProfile();
        this.EditCrowdDivision();
        this.EditCompanyName();
        this.EditLocation();
        this.EditWebsite();
        this.EditAbout();
        this.saveScreenshotPNG(driver);
}
    }
