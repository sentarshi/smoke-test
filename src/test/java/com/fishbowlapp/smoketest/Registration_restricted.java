package com.fishbowlapp.smoketest;//package <set your test package>;

import com.fishbowlapp.framework.MicroFramewok;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


public class Registration_restricted{
    protected AndroidDriver<AndroidElement> driver = null;
    private MicroFramewok drHacks = null;


    public Registration_restricted(AndroidDriver<AndroidElement> driverinput) {

        driver = driverinput;
        drHacks = new MicroFramewok(driver);
    }

    @Step("Initial screen pass")
    public void InitialScreen() throws InterruptedException  {
        driver.resetApp();
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/buttonTextView"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/buttonTextView")).click();
    }

    @Step("Optional Auto phone")
    public void SkipPhone() throws InterruptedException {
        MicroFramewok.WaitForElement(driver,By.id("com.google.android.gms:id/cancel"));
        driver.findElement(By.id("com.google.android.gms:id/cancel")).click();
    }

    @Step("Enter Phone number screen")
    public void PhoneNumberScreen () {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/editText"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/editText")).sendKeys("2345");
        driver.findElement(By.xpath("//*[@text='Continue']")).click();
    }

    @Step ("Verification Code Screen")
    public void VerificationPhoneScreen() {
        new WebDriverWait(driver, 50).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.EditText")));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_2));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_3));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_4));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_5));
    }
    @Step ("Verify you are professional")
    public void ChooseLoginType (){
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/verifyWithCompanyEmailTextView"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/verifyWithCompanyEmailTextView")).click();
    }
    @Step("Enter Email Screen")
    public void EnterEmailScreen() {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/editText"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/editText")).click();
        String mail = "test" + Math.random() + "@kk.com";
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/editText")).setValue(mail);
        driver.pressKey(new KeyEvent(AndroidKey.ENTER));
    }

    @Step("Verification Email Screen")
    public void EmailVerificationScreen() {
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
    }

    @Step("Choose Crowd and Division")
    public void ChooseCrowddivision() {
        new WebDriverWait(driver, 3).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='Consulting']")));
        driver.findElement(By.xpath("//*[@text='Consulting']")).click();
        new WebDriverWait(driver, 3).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='Strategy']")));
        driver.findElement(By.xpath("//*[@text='Strategy']")).click();
        new WebDriverWait(driver, 3).until(ExpectedConditions.presenceOfElementLocated(By.id("com.fishbowlmedia.fishbowl.dev:id/md_buttonDefaultPositive")));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/md_buttonDefaultPositive")).click();
    }

    @Step("EnterFirst and Last Name")
    public void EnterFirstLastName() {
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/firstNameEditText")).sendKeys("test1");
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/lastNameEditText")).sendKeys("test2");
        driver.pressKey(new KeyEvent(AndroidKey.TAB));
        driver.pressKey(new KeyEvent(AndroidKey.ENTER));
    }

    @Step("Enter Title")
    public void EnterTitle() {
        new WebDriverWait(driver, 3).until(ExpectedConditions.presenceOfElementLocated(By.id("com.fishbowlmedia.fishbowl.dev:id/editText")));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/editText")).sendKeys("HR");
        driver.pressKey(new KeyEvent(AndroidKey.ENTER));
    }

    @Step("Select BrithDate")
    public void BirthDate() {
        new WebDriverWait(driver, 3).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.NumberPicker[3]//android.widget.Button")));
        int i = 0;
        while (i < 21) {
            driver.findElement(By.xpath("//android.widget.NumberPicker[3]//android.widget.Button")).click();
            i++;
        }
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/continueButton")).click();
    }

    @Step("Choose Gender")
    public void ChooseGender() {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/maleImageView"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/maleImageView")).click();
        new WebDriverWait(driver, 3).until(ExpectedConditions.presenceOfElementLocated(By.id("com.fishbowlmedia.fishbowl.dev:id/createPostTextView")));
    }

    @Step("Tutorial")
    public void Tutorial() throws InterruptedException, IOException {
        //new WebDriverWait(driver, 3).until(ExpectedConditions.presenceOfElementLocated(By.id("com.fishbowlmedia.fishbowl.dev:id/createPostTextView")));
        Thread.sleep(4000);
        driver.findElement(MobileBy.AccessibilityId("Let's Go")).click();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.findElement(MobileBy.AccessibilityId("Next")).click();
        //MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/createPostTextView")));
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.findElement(MobileBy.AccessibilityId("View Contacts")).click();
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/permission_allow_button"))); //permission_allow_button
        driver.findElement(By.id("com.android.packageinstaller:id/permission_deny_button")).click(); //permission_allow_button
        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("com.fishbowlmedia.fishbowl.dev:id/rootView")));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/rootView")).click();
        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("com.fishbowlmedia.fishbowl.dev:id/ok_tv")));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/ok_tv")).click();
        //driver.findElement(MobileBy.AccessibilityId("Bowl")).click();
        AndroidTouchAction action = new AndroidTouchAction(driver);
        action.tap(TapOptions.tapOptions().withElement(ElementOption.element(driver.findElement(MobileBy.AccessibilityId("Bowl"))))).perform();
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/okButton")).click();
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/explore_fishbowl_tv"));
        String result = driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/explore_fishbowl_tv")).getAttribute("text");
        Assert.assertEquals(result, "Explore Fishbowl");
        this.saveScreenshotPNG(driver);
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/explore_fishbowl_tv")).click();

    }

    @Test
    @Attachment (value = "Page screenshot", type = "image/png")
    public byte[] saveScreenshotPNG(WebDriver driver){
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);

    }
    @Step ("Registration Standard Flow")
    public void TestAssemble () throws InterruptedException, IOException {
        this.InitialScreen();
        try {
            this.SkipPhone();
        }
        catch (Exception e)
        {
            System.out.println("No phone number displayed");
        }
        this.PhoneNumberScreen();
        this.VerificationPhoneScreen();
        this.ChooseLoginType();
        this.EnterEmailScreen();
        this.EmailVerificationScreen();
        this.ChooseCrowddivision();
        this.EnterFirstLastName();
        this.EnterTitle();
        this.BirthDate();
        this.ChooseGender();
        this.Tutorial();

    }

    @AfterMethod
    public void tearDown() {

        driver.quit();
    }
}