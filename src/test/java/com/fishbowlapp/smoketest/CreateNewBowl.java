package com.fishbowlapp.smoketest;

import com.fishbowlapp.framework.MicroFramewok;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class CreateNewBowl {
    protected AndroidDriver<AndroidElement> driver = null;
    public CreateNewBowl(AndroidDriver<AndroidElement> driverinput) {
        driver = driverinput;}
    @Attachment(value = "Page screenshot", type = "image/png")
    public byte[] saveScreenshotPNG(WebDriver driver){
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
    }


    @Step("GotoBowlsScreen")
    private void GoToBowlScreen() throws InterruptedException  {
        MicroFramewok.WaitForElement(driver,By.xpath("//*[@text='Bowls']"));
        driver.findElement(By.xpath("//*[@text='Bowls']")).click();
}
    @Step("CreateBowl")
    private void CreateBowl() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        new MicroFramewok().VerticalSwipeDownUp(driver,1);
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        driver.getPageSource();
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/create_bowl_ll"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/create_bowl_ll")).click();
    }
    @Step("CustomBowl")
    private void CustiomBowl() throws InterruptedException {
        MicroFramewok.WaitForElement(driver,By.xpath("//android.widget.LinearLayout[6]//android.widget.ImageView"));
        driver.findElement(By.xpath("//android.widget.LinearLayout[6]//android.widget.ImageView")).click();
    }
    @Step("NameBowlAndDirectory")
    private void NameBowlAndDirectory() throws InterruptedException {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/bowlTypeSwitch"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/bowlTypeSwitch")).click();
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/editText")).sendKeys("TestAutoBowl");
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/done_menu")).click();
    }
    @Step("AddContactsSkip")
    private void AddContactSkip() throws InterruptedException {
        MicroFramewok.WaitForElement(driver,By.xpath("//*[@text='DONE']"));
        driver.findElement(By.xpath("//*[@text='DONE']")).click();
    }

    @Step("DescriptionStep")
    private void DescriptionStep() throws InterruptedException {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/descriptionEditText"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/descriptionEditText")).sendKeys("This is custom automated test bowl");
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/descriptionEditText")).click();
        driver.pressKey(new KeyEvent(AndroidKey.ENTER));
    }
    @Step
    private void Verify() throws InterruptedException{
        new MicroFramewok().VerticalSwipeDownUp(driver,1);
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/send_welcome_msg_tv"));
        String result = driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/send_welcome_msg_tv")).getAttribute("text");
        Assert.assertEquals(result,"Send Welcome Message");
    }


@Test
@Step("New Custom bowl creation")
    public void TestAssemble() throws InterruptedException {
        this.GoToBowlScreen();
        this.CreateBowl();
        this.CustiomBowl();
        this.NameBowlAndDirectory();
        this.AddContactSkip();
        this.saveScreenshotPNG(driver);
        this.DescriptionStep();
       // this.Verify();
        this.saveScreenshotPNG(driver);
    }
        }