package com.fishbowlapp.smoketest;

import com.fishbowlapp.framework.MicroFramewok;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.qameta.allure.Attachment;
import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.openqa.selenium.*;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class DM_Test {

    protected AndroidDriver<AndroidElement> driver = null;
    public DM_Test(AndroidDriver<AndroidElement> driverinput) {
        driver = driverinput;}
    @Attachment(value = "Page screenshot", type = "image/png")
    public byte[] saveScreenshotPNG(WebDriver driver){
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);

    }
    @Step("Start a DM")
    private void DmStart() throws InterruptedException {
        MicroFramewok.WaitForElement(driver, By.id("com.fishbowlmedia.fishbowl.dev:id/messages_container_rl"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/messages_container_rl")).click();
        MicroFramewok.WaitForElement(driver, By.id("com.fishbowlmedia.fishbowl.dev:id/go_to_explore_tv"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/go_to_explore_tv")).click();
        MicroFramewok.WaitForElement(driver,MobileBy.AccessibilityId("people"));
        driver.findElement(MobileBy.AccessibilityId("people"));
        for (int i =1; i < 100;i++){
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            new MicroFramewok().VerticalSwipe(driver,1);
           MicroFramewok.WaitForElement(driver, MobileBy.AccessibilityId("2"));
            driver.findElement(MobileBy.AccessibilityId("2")).click();
            driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/start_chatting_tv")).click();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

            if (MicroFramewok.TryCElementPresent(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/md_buttonDefaultPositive"))) {

                driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/md_buttonDefaultPositive")).click();
                MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/cancel_tv"));
                driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/cancel_tv")).click();
            }
            else
            {
                i = 100;
                
            }
        }
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/chat_system_message_tv"));
        this.saveScreenshotPNG(driver);
    }

    @Step("Cancel DM")
    private void CancelDM() throws InterruptedException {
    driver.findElement(MobileBy.AccessibilityId("Navigate up")).click();
    MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/md_buttonDefaultNegative"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/md_buttonDefaultNegative")).click();
    }
    @Step("Send a message")
    private void SendMessage() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
            driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/start_chatting_tv")).click();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/message_et"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/message_et")).click();
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/message_et")).sendKeys("Sup Dude!");
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/send_tv")).click();
        String result = driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/user_chat_message_tv")).getAttribute("text");
        Assert.assertEquals(result, "Sup Dude!");
        this.saveScreenshotPNG(driver);
    }

    @Step("Check DM List")
    private void DMList() throws InterruptedException {
        driver.findElement(MobileBy.AccessibilityId("Navigate up")).click();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.findElement(MobileBy.AccessibilityId("Navigate up")).click();
       // driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/cancel_tv")).click();
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/dm_icon_iv")).click();
        String result = driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/last_message_tv")).getAttribute("text");
        Assert.assertEquals(result, "You: Sup Dude!");
        this.saveScreenshotPNG(driver);
    }
    @Test
    @Description("Start Cancel and Check DMs")
    public void TestAssembly() throws InterruptedException, IOException {
        this.DmStart();
        this.CancelDM();
        this.SendMessage();
        this.DMList();
    }



}
