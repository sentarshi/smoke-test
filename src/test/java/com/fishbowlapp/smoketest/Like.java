package com.fishbowlapp.smoketest;

import com.fishbowlapp.framework.MicroFramewok;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.qameta.allure.Attachment;
import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import java.io.IOException;

public class Like {
    protected AndroidDriver<AndroidElement> driver = null;

    String posttext = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.";

    public Like(AndroidDriver<AndroidElement> driverinput) {
        driver = driverinput;
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    public byte[] saveScreenshotPNG(WebDriver driver){
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
    }

    @Step("Comment on post")
    public void CommentPost() throws InterruptedException {
        //driver.getPageSource();
        new MicroFramewok().VerticalSwipeDownUp(driver,1);
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/replies_counts_tv"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/replies_counts_tv")).click();
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/compose_title_tv")).click();
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/message_et")).sendKeys(posttext);
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/publish_tv")).click();
    }
    @Step("Like on Post")
    public void LikePost() throws InterruptedException {
        AndroidTouchAction action = new AndroidTouchAction(driver);
        action.longPress(LongPressOptions.longPressOptions().withElement(ElementOption.element(driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/image_like_iv_post"))))).perform();
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/uplifting")).click();

    }
    @Step("Reply on Comment")
    public void CommentRply() throws InterruptedException {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/reply_tv"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/reply_tv")).click();
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/message_et")).sendKeys(posttext);
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/publish_tv")).click();
    }
    @Step("Edit Re Reply message")
    public void EditReReply() {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/more_ic"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/more_ic")).click();
        driver.findElement(By.xpath("//*[@text='Edit Message']")).click();
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/message_et")).sendKeys("EDITED RE REPLY HERE");
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/publish_tv")).click();
    }
    @Step("Like on reply")
    public void LikeRply() throws InterruptedException, IOException {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/image_like_iv_reply"));
        AndroidTouchAction action = new AndroidTouchAction(driver);
        action.longPress(LongPressOptions.longPressOptions().withElement(ElementOption.element(driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/image_like_iv_reply"))))).perform();
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/helpful")).click();
    }

    @Step("Like on rereply")
        public void LikeReRply() throws InterruptedException, IOException {
            AndroidTouchAction action = new AndroidTouchAction(driver);
            MicroFramewok.WaitForElement(driver,MobileBy.AccessibilityId("sub_comment_1"));
            action.longPress(LongPressOptions.longPressOptions().withElement(ElementOption.element(driver.findElement(MobileBy.AccessibilityId("sub_comment_1"))))).perform();
            driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/funny")).click();
    }

    @Step("CHange Reaction on RE Reply")
    public void ChangeReactiononReReply() throws InterruptedException, IOException {
        AndroidTouchAction action = new AndroidTouchAction(driver);
        action.longPress(LongPressOptions.longPressOptions().withElement(ElementOption.element(driver.findElement(MobileBy.AccessibilityId("sub_comment_1"))))).perform();
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/like")).click();
        this.saveScreenshotPNG(driver);
    }
    @Test
    @Description("Create comments and Likes")
    public void TestAssembly() throws InterruptedException, IOException {

        this.CommentPost();
        this.LikePost();
        this.CommentRply();
        this.LikeRply();
        this.LikeReRply();
        this.ChangeReactiononReReply();
        this.EditReReply();

    }
}