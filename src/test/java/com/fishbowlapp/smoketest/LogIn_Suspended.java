package com.fishbowlapp.smoketest;//package <set your test package>;

import com.fishbowlapp.framework.MicroFramewok;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


public class LogIn_Suspended{
    protected AndroidDriver<AndroidElement> driver = null;
    private MicroFramewok drHacks = null;


    public LogIn_Suspended(AndroidDriver<AndroidElement> driverinput) {

        driver = driverinput;
        drHacks = new MicroFramewok(driver);
    }

    @Step("Initial screen pass")
    public void InitialScreen() throws InterruptedException  {
        driver.resetApp();
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/buttonTextView"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/buttonTextView")).click();
    }

    @Step("Optional Auto phone")
    public void SkipPhone() throws InterruptedException {
        MicroFramewok.WaitForElement(driver,By.id("com.google.android.gms:id/cancel"));
        driver.findElement(By.id("com.google.android.gms:id/cancel")).click();
    }

    @Step("Enter Phone number screen")
    public void PhoneNumberScreen () {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/editText"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/editText")).sendKeys("2345");
        driver.findElement(By.xpath("//*[@text='Continue']")).click();
    }

    @Step ("Verification Code Screen")
    public void VerificationPhoneScreen() throws InterruptedException {
        MicroFramewok.WaitForElement(driver,By.xpath("//android.widget.EditText"));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_2));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_3));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_4));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_5));
    }
    @Step ("Verify you are professional")
    public void ChooseLoginType (){
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/verifyWithCompanyEmailTextView"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/verifyWithCompanyEmailTextView")).click();
    }
    @Step("Enter Email Screen")
    public void EnterEmailScreen() {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/editText"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/editText")).click();
        String mail = "banneduser@kk.com";
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/editText")).setValue(mail);
        driver.pressKey(new KeyEvent(AndroidKey.ENTER));
    }

    @Step("Verification Email Screen")
    public void EmailVerificationScreen() {
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
    }

    @Step("Tutorial")
    public void Tutorial() throws InterruptedException, IOException {
        Thread.sleep(4000);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/banned_title_tv")).isDisplayed();
        this.saveScreenshotPNG(driver);

    }

    @Test
    @Attachment (value = "Page screenshot", type = "image/png")
    public byte[] saveScreenshotPNG(WebDriver driver){
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);

    }
    @Step ("Registration Login Suspended")
    public void TestAssemble () throws InterruptedException, IOException {
        this.InitialScreen();
        try {
            this.SkipPhone();
        }
        catch (Exception e)
        {
            System.out.println("No phone number displayed");
        }
        this.PhoneNumberScreen();
        this.VerificationPhoneScreen();
        this.ChooseLoginType();
        this.EnterEmailScreen();
        this.EmailVerificationScreen();
        this.Tutorial();
    }

    @AfterMethod
    public void tearDown() {

        driver.quit();
    }
}