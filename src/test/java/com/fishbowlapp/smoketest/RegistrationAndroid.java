package com.fishbowlapp.smoketest;//package <set your test package>;

import com.fishbowlapp.framework.MicroFramewok;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


public class RegistrationAndroid {
    protected AndroidDriver<AndroidElement> driver = null;
   private MicroFramewok drHacks = null;


    public RegistrationAndroid(AndroidDriver<AndroidElement> driverinput) {

        driver = driverinput;
        drHacks = new MicroFramewok(driver);
    }

    @Step("Initial screen pass1")
    public void InitialScreen() throws InterruptedException  {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/professional_btn_tv"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/professional_btn_tv")).click();
    }

    @Step("Initial screen pass2")
    public void InitialScreen2() throws InterruptedException  {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/buttonTextView"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/buttonTextView")).click();
    }
    @Step("Optional Auto phone")
    public void SkipPhone() throws InterruptedException {
        MicroFramewok.WaitForElement(driver,By.id("com.google.android.gms:id/cancel"));
        driver.findElement(By.id("com.google.android.gms:id/cancel")).click();
    }

    @Step("Enter Phone number screen")
    public void PhoneNumberScreen () {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/editText"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/editText")).sendKeys("2345");
        driver.findElement(By.xpath("//*[@text='Continue']")).click();
    }

    @Step ("Verification Code Screen")
    public void VerificationPhoneScreen() {
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        new WebDriverWait(driver, 50).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.EditText")));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_2));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_3));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_4));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_5));
    }


    @Step ("Verify you are professional")
        public void ChooseLoginType (){
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/verify_email_tv"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/verify_email_tv")).click();
    }
    @Step("Enter Email Screen")
    public void EnterEmailScreen() {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/editText"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/editText")).click();
        String mail = "test" + Math.random() + "@kk.com";
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/editText")).setValue(mail);
        driver.pressKey(new KeyEvent(AndroidKey.ENTER));
    }

    @Step("Verification Email Screen")
     public void EmailVerificationScreen() {
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/editText"));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
        driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
    }

    @Step("Optional PYMK screen")
    public void PYMK() throws InterruptedException  {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/verify_professional_tv"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/verify_professional_tv")).click();}


    @Step("EnterFirst and Last Name")
    public void EnterFirstLastName() {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/firstNameEditText"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/firstNameEditText")).sendKeys("test1");
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/lastNameEditText")).sendKeys("test2");
        driver.pressKey(new KeyEvent(AndroidKey.TAB));
        driver.pressKey(new KeyEvent(AndroidKey.ENTER));
    }

    @Step("Choose Crowd and Division")
    public void ChooseCrowddivision() {
        new WebDriverWait(driver, 3).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='Consulting']")));
        driver.findElement(By.xpath("//*[@text='Consulting']")).click();
        new WebDriverWait(driver, 3).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='Strategy']")));
        driver.findElement(By.xpath("//*[@text='Strategy']")).click();
        new WebDriverWait(driver, 3).until(ExpectedConditions.presenceOfElementLocated(By.id("com.fishbowlmedia.fishbowl.dev:id/md_buttonDefaultPositive")));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/md_buttonDefaultPositive")).click();
    }



    @Step("Enter Title")
    public void EnterTitle() {
        new WebDriverWait(driver, 3).until(ExpectedConditions.presenceOfElementLocated(By.id("com.fishbowlmedia.fishbowl.dev:id/editText")));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/editText")).sendKeys("QA1123");
        driver.pressKey(new KeyEvent(AndroidKey.ENTER));
    }

    @Step("Select BrithDate")
    public void BirthDate() {
        new WebDriverWait(driver, 3).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.NumberPicker[3]//android.widget.Button")));
        int i = 0;
        while (i < 21) {
            driver.findElement(By.xpath("//android.widget.NumberPicker[3]//android.widget.Button")).click();
            i++;
        }
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/continueButton")).click();
    }

    @Step("Choose Gender")
    public void ChooseGender() {
        MicroFramewok.WaitForElement(driver,By.id("com.fishbowlmedia.fishbowl.dev:id/maleImageView"));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/maleImageView")).click();
        //new WebDriverWait(driver, 3).until(ExpectedConditions.presenceOfElementLocated(By.id("com.fishbowlmedia.fishbowl.dev:id/createPostTextView")));
    }

    @Step("Tutorial")
    public void Tutorial() throws InterruptedException, IOException {
        //new WebDriverWait(driver, 3).until(ExpectedConditions.presenceOfElementLocated(By.id("com.fishbowlmedia.fishbowl.dev:id/createPostTextView")));
        Thread.sleep(10000);
        driver.findElement(MobileBy.AccessibilityId("Let's Go")).click();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.findElement(MobileBy.AccessibilityId("Next")).click();
        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("com.fishbowlmedia.fishbowl.dev:id/rootView")));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/rootView")).click();
        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("com.fishbowlmedia.fishbowl.dev:id/ok_tv")));
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/ok_tv")).click();
        //driver.findElement(MobileBy.AccessibilityId("Bowl")).click();
        AndroidTouchAction action = new AndroidTouchAction(driver);
        action.tap(TapOptions.tapOptions().withElement(ElementOption.element(driver.findElement(MobileBy.AccessibilityId("Bowl"))))).perform();
        driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/okButton")).click();
       // new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("com.fishbowlmedia.fishbowl.dev:id/tooltipText")));
       // driver.findElement(By.id("com.fishbowlmedia.fishbowl.dev:id/tooltipText")).click();
    }

    @Test
    @Attachment (value = "Page screenshot", type = "image/png")
    public byte[] saveScreenshotPNG(WebDriver driver){
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);

    }
    @Step ("Registration Standard Flow")
    public void TestAssemble () throws InterruptedException, IOException {
     this.InitialScreen();
     this.InitialScreen2();
     try {
         this.SkipPhone();
     }
     catch (Exception e)
     {
         System.out.println("No phone number displayed");
     }
     this.PhoneNumberScreen();
     this.VerificationPhoneScreen();
     this.ChooseLoginType();
     this.EnterEmailScreen();
     this.EmailVerificationScreen();
        try {
            this.PYMK();
        }
        catch (Exception e)
        {
            System.out.println("No PYMK screen");
        }
     this.EnterFirstLastName();
     //this.ChooseCrowddivision();
     this.EnterTitle();
     this.BirthDate();
     this.ChooseGender();
     this.Tutorial();
     this.saveScreenshotPNG(driver);
    }

    @AfterMethod
    public void tearDown() {

        driver.quit();
    }
}